package com.brian_jesse.picupload;

import javax.ejb.Stateless;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
@Named
public class UploadImageController {

    @PersistenceContext(unitName="uploadImagePU")
    private EntityManager em;

    public UploadImage getImageById(Long imageId) {
        return em.find(UploadImage.class, imageId);
    }

    public UploadImage saveImage(UploadImage image) {
        em.persist(image);
        return image;
    }
}
