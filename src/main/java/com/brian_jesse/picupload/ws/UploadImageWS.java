package com.brian_jesse.picupload.ws;

import java.io.IOException;
import java.io.InputStream;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.brian_jesse.picupload.UploadImage;
import com.brian_jesse.picupload.UploadImageController;
import com.google.common.io.ByteStreams;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;

@Stateless
@Path("image")
public class UploadImageWS {

    @Inject
    UploadImageController uploadImageController;

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response saveImage(@FormDataParam("image") InputStream imageStream, @FormDataParam("image") FormDataContentDisposition imageDetail) {
        String type = imageDetail.getFileName();
        type = type.substring(type.lastIndexOf('.') + 1);
        if(type.equals("jpg")) {
            type = "jpeg";
        }
        if(!type.equals("png") && !type.equals("jpeg") && !type.equals("gif")) {
            return Response.noContent().build();
        }
        byte[] image = null;
        try {
            image = ByteStreams.toByteArray(imageStream);
        } catch (IOException e) {
            e.printStackTrace();
            return Response.serverError().build();
        }
        UploadImage uploadImage = new UploadImage();
        uploadImage.setImage(image);
        uploadImage.setImageType(type);
        uploadImage = uploadImageController.saveImage(uploadImage);
        return Response.ok().build();
    }

    @GET
    @Path("{imageId}")
    @Produces("image/*")
    public Response getImage(@PathParam("imageId") Long imageId) {
        UploadImage uploadImage = uploadImageController.getImageById(imageId);
        String imageType = "image/" + uploadImage.getImageType();
        return Response.ok(uploadImage.getImage(), imageType).build();
    }
}
