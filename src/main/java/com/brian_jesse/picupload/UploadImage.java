package com.brian_jesse.picupload;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@EqualsAndHashCode
public class UploadImage implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    @Lob
    @Basic(fetch=FetchType.LAZY)
    @Column(length=16777215)
    private byte[] image;

    private String imageType;
}
